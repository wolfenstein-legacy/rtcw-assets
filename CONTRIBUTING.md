We consider ourselves as an international community of developers and users keeping Return To Castle Wolfenstein alive and up to date.   
Our development is a collaborative effort done in an open, transparent and friendly manner.  
Anyone is welcome to join our efforts!

To get started contributing, you need to clone the repository. [Please follow this to get started][1].

[1]: https://gitlab.com/wolfenstein-legacy/et-legacy/wikis/how-to-setup-git