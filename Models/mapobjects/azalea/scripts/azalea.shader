models/mapobjects/azalea/azaleaflowers_1
{
	cull twosided
	surfaceparm alphashadow
	{
		map models/mapobjects/azalea/azaleaflowers_1.tga
		alphaFunc GE128
		rgbGen identity
		rgbGen identity
	}
}

models/mapobjects/azalea/azaleabranches
{
	cull twosided
	surfaceparm alphashadow
	{
		map models/mapobjects/azalea/azaleabranches.tga
		alphaFunc GE128
		rgbGen identity
	}
}

models/mapobjects/azalea/azaleaflowers_2
{
	cull twosided
	surfaceparm alphashadow
	{
		map models/mapobjects/azalea/azaleaflowers_2.tga
		alphaFunc GE128
		rgbGen identity
	}
}
