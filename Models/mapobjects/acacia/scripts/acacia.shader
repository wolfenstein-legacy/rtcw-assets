models/mapobjects/acacia/acacia_leaves_1
{
	cull twosided
	surfaceparm alphashadow
	{
		map models/mapobjects/acacia/acacia_leaves_1.tga
		alphaFunc GE128
		rgbGen identity
		rgbGen identity
	}
}

models/mapobjects/acacia/acacia_leaves_2
{
	cull twosided
	surfaceparm alphashadow
	{
		map models/mapobjects/acacia/acacia_leaves_2.tga
		alphaFunc GE128
		rgbGen identity
	}
}

models/mapobjects/acacia/acacia_branches
{
	cull twosided
	surfaceparm alphashadow
	{
		map models/mapobjects/acacia/acacia_branches.tga
		alphaFunc GE128
		rgbGen identity
	}
}

models/mapobjects/acacia/acacia_thorn
{
	cull twosided
	surfaceparm alphashadow
	{
		map models/mapobjects/acacia/acacia_branches.tga
		alphaFunc GE128
		rgbGen identity
	}
}
